import requests
import json
from tools.preProcessing import filterAllTexts
from tools.JsonCRUD import read, save


url = "http://localhost:18881/completion"
headers = {"Content-Type": "application/json"}

query1_woh = "[INST]Write a summary about the character {character} in the book {book}.[/INST]"
query1_wh = "[INST]Write a summary about the character {character} in the given text passages: \n {help}[/INST]"


query2_woh = "[INST]Write a summary in the style of a fandom article about the character {character} in the book {book}.[/INST]"
query2_wh = "[INST]Write a summary in the style of a fandom article about the character {character} in the given text passages: \n {help}[/INST]"

query3_woh = "[INST]Provide a concise overview of the character {character} from the book {book}.[/INST]"
query3_wh = "[INST]Provide a concise overview of the character {character} based on the following excerpts: \n {help}[/INST]"

query4_woh = "[INST]rite sumary bout thee cara cter  {character} of th book {book}.[/INST]"
query4_wh = "[INST]rite sumary bout thee cara cter  {character} bsed th fllowing excerpts: \n {help}[/INST]"

def query_without_help(book, character):
    data = {"prompt": query4_woh.format(character=character, book=book), "temperature" : 0.0} #, "temperature" : 0.0
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response.json()

def query_with_help(help, character):
    data = {"prompt": query4_wh.format(character=character, help=help), "temperature" : 0.0}
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response.json()







####################################################
# passages = filterAllTexts(bks["Harry Potter"], "Seamus Finnigan")
# print(passages)
####################################################
# LOTR Boromir
# Harry Potter 2 Remus Lupin 0
# Harry Potter 2 Minerva McGonagall 1
# Harry Potter 2 Katie Bell 27


def generateFandoms(fandoms, books, file_name, from_book=1, from_character=0):

    generated_fandoms = read(file_name)

    for index, book in enumerate(fandoms):
        if index >= from_book:
            if not book in generated_fandoms:
                generated_fandoms[book] = {}
            for ind, character in enumerate(fandoms[book]):
                if ind >= from_character and not character in generated_fandoms[book]:
                    print("trying", book, index , character, ind)
                    passages = filterAllTexts(books[book], character)
                    res1 = query_without_help(book, character)
                    res2 = query_with_help(passages, character)
                    generated_fandoms[book][character] = {}
                    generated_fandoms[book][character]["without"] = res1["content"]
                    generated_fandoms[book][character]["with"] = res2["content"]
                    generated_fandoms[book][character]["passages"] =  passages
                    save(generated_fandoms, file_name)
                    print(index, ind)
                    print(book, character)


fandoms = read('new_fandom.json')
books= read("kolo.json")
generateFandoms(fandoms, books, "gen_fand4.json")


















