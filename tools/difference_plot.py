import pandas as pd
import matplotlib.pyplot as plt

def process_and_sort(csv):
    data = pd.read_csv(csv)

    # Calculate improvement
    data['F1_to_wF1'] = data['wF1'] - data['F1']
    data['h_to_wh'] = data['wh'] - data['h']

    
    # Sort based on improvement values
    sorted_by_F1_to_wF1 = data.sort_values(by='F1_to_wF1')
    sorted_by_h_to_wh = data.sort_values(by='h_to_wh')

    return sorted_by_F1_to_wF1, sorted_by_h_to_wh

def plot_improvements(sorted_data, title, ylabel, column):
    plt.figure(figsize=(10, 5))
    plt.plot(sorted_data[column], label=title)
    plt.xlabel('Entries')
    plt.ylabel(ylabel)
    plt.title(title)
    plt.legend()
    plt.show()

# List of CSV files
csv_files = [
    'results/results.csv',
    'results/results2.csv',
    'results/results3.csv',
    'results/results4.csv'
]

# Process each CSV file and plot the results
for index, csv in enumerate(csv_files):
    sorted_by_F1_to_wF1, sorted_by_h_to_wh = process_and_sort(csv)
    
    # Save the sorted data if needed
    sorted_by_F1_to_wF1.to_csv(f'embed_sorted_by_F1_to_wF1_{index}.csv', index=False)
    sorted_by_h_to_wh.to_csv(f'embed_sorted_by_h_to_wh_{index}.csv', index=False)
    
    # Plot the improvements for F1 to wF1
    plot_improvements(sorted_by_F1_to_wF1, f'Improvement from F1 to wF1 - {csv}', 'Improvement Value', 'F1_to_wF1')
    
    # Plot the improvements for h to wh
    plot_improvements(sorted_by_h_to_wh, f'Improvement from h to wh - {csv}', 'Improvement Value', 'h_to_wh')
