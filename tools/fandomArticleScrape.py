
import requests
from bs4 import BeautifulSoup


def fetchFandomArticle(response):
    wholeArticle = scrapeFandom(response)
    return wholeArticle


def scrapeFandom(response):
    data = []


    soup = BeautifulSoup(response.text, 'html.parser')
    content_div = soup.find('div', {'class': 'mw-parser-output'})
    paragraphs = content_div.find_all('p', class_=False, recursive=False)
    for paragraph in paragraphs:
        for sup in paragraph.find_all('sup'):
            sup.extract()
        data.append(paragraph.text)
    return data

def checkUrl(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'}
    data =None
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            data = response
    except requests.RequestException as e:
        print(f"Request Exception: {e}")
    return  data
    
