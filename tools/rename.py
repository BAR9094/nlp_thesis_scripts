import re

pattern = r"\n\n\d+\n\n\n\n[\u2019a-zA-Z' !,-]{5,50}\n\n"  # Simplified to match "C H A P T E R" followed by digits
text = "\n\n342\n\n\n\nBathilda\u2019s Secret\n\n"

match = re.search(pattern, text)
if match:
    print("Match found:", match.group())
else:
    print("No match found")
