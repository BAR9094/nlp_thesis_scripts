import pandas as pd
from scipy import stats

# Load your data from a CSV file
def test(csv):
    data = pd.read_csv(csv)

    # Extract the 'before' and 'after' columns
    before = data['F1']
    after = data['wF1']

    before2 = data['h']
    after2 = data['wh']

    # Perform a dependent (paired) T-Test
    t_stat1, p_value1 = stats.ttest_rel(before, after)
    print(f"Dependent T-Test (F1 vs wF1): t-statistic = {t_stat1}, p-value = {p_value1}")

    # Perform a Spearman correlation
    spearman_corr1, spearman_p_value1 = stats.spearmanr(before, after)
    print(f"Spearman Correlation (F1 vs wF1): correlation coefficient = {spearman_corr1}, p-value = {spearman_p_value1}")
    
    # Perform a dependent (paired) T-Test
    t_stat2, p_value2 = stats.ttest_rel(before2, after2)
    print(f"Dependent T-Test (h vs wh): t-statistic = {t_stat2}, p-value = {p_value2}")

    # Perform a Spearman correlation
    spearman_corr2, spearman_p_value2 = stats.spearmanr(before2, after2)
    print(f"Spearman Correlation (h vs wh): correlation coefficient = {spearman_corr2}, p-value = {spearman_p_value2}")

    print(f"$P_{{1}}$ & BLEU & {round(t_stat1,2)} ({'%10.2e' % (p_value1)}) & {round(spearman_corr1,2)} ({'%10.2e' % (spearman_p_value1)}) \\\\ ")
    print(f"$P_{{1}}$ & BERT & {round(t_stat2,2)} ({'%10.2e' % (p_value2)}) & {round(spearman_corr2,2)} ({'%10.2e' % (spearman_p_value2)}) \\\\ ")


def test2(csv1, csv2):
    data1 = pd.read_csv(csv1)
    data2 = pd.read_csv(csv2)

    merged_data = pd.merge(data1, data2, on='Character', suffixes=('_1', '_2'))

    # Extract the 'before' and 'after' columns
    before = merged_data['F1_1']
    after = merged_data['F1_2']

    before2 = merged_data['wF1_1']
    after2 = merged_data['wF1_2']

    before3 = merged_data['h_1']
    after3 = merged_data['h_2']

    before4 = merged_data['wh_1']
    after4 = merged_data['wh_2']

    # Perform a dependent (paired) T-Test and Spearman correlation for each pair
    t_stat, p_value = stats.ttest_rel(before, after)
    spearman_corr1, spearman_p_value1 = stats.spearmanr(before, after)


    t_stat1, p_value1 = stats.ttest_rel(before2, after2)
    spearman_corr2, spearman_p_value2 = stats.spearmanr(before2, after2)


    t_stat2, p_value2 = stats.ttest_rel(before3, after3)
    spearman_corr3, spearman_p_value3 = stats.spearmanr(before3, after3)


    t_stat3, p_value3 = stats.ttest_rel(before4, after4)
    spearman_corr4, spearman_p_value4 = stats.spearmanr(before4, after4)

    print(f"$P_{{1}}$ & BLEU & {round(t_stat,2)} ({'%10.2e' % (p_value)}) & {round(spearman_corr1,2)} ({'%10.2e' % (spearman_p_value1)}) \\\\ ")

    print(f"$P_{{1}}$ & BERT & {round(t_stat1,2)} ({'%10.2e' % (p_value1)}) & {round(spearman_corr2,2)} ({'%10.2e' % (spearman_p_value2)}) \\\\ ")

    print(f"$P_{{1}}$ & BERT & {round(t_stat2,2)} ({'%10.2e' % (p_value2)}) & {round(spearman_corr3,2)} ({'%10.2e' % (spearman_p_value3)}) \\\\ ")

    print(f"$P_{{1}}$ & BERT & {round(t_stat3,2)} ({'%10.2e' % (p_value3)}) & {round(spearman_corr4,2)} ({'%10.2e' % (spearman_p_value4)}) \\\\ ")


def test_threshold(csv1, csv2, mask, threshold_value):
    data1 = pd.read_csv(csv1)
    data2 = pd.read_csv(csv2)
    data3 = pd.read_csv(mask)
    # filtered_df = data3.groupby('Character')['Score'].max().reset_index()
    # filtered_df = filtered_df[filtered_df['Score'] < threshold_value]
    
    filtered_df = data3.groupby('Character')['Score'].min().reset_index()
    filtered_df = filtered_df[filtered_df['Score'] > threshold_value]
    
    mata = pd.merge(data1, data2, on='Character', suffixes=('_1', '_2'))
    print(len(mata))
    merged_data = pd.merge(mata, filtered_df, on='Character')
    print(len(merged_data))



    ################################


    # merged_df = pd.merge(mata, filtered_df, on='Character', how='outer', indicator=True)

    # # Rows only in data1
    # only_in_data1 = merged_df[merged_df['_merge'] == 'left_only']['Character']
    # print("Rows only in data1:")
    # print(only_in_data1)

    # # Rows only in data2
    # only_in_data2 = merged_df[merged_df['_merge'] == 'right_only']['Character']
    # print("Rows only in data2:")
    # print(only_in_data2)
    ##################################



    # Extract the 'before' and 'after' columns
    before = merged_data['F1_1']
    after = merged_data['F1_2']

    before2 = merged_data['wF1_1']
    after2 = merged_data['wF1_2']

    before3 = merged_data['h_1']
    after3 = merged_data['h_2']

    before4 = merged_data['wh_1']
    after4 = merged_data['wh_2']

    # Perform a dependent (paired) T-Test and Spearman correlation for each pair
    t_stat, p_value = stats.ttest_rel(before, after)
    spearman_corr1, spearman_p_value1 = stats.spearmanr(before, after)


    t_stat1, p_value1 = stats.ttest_rel(before2, after2)
    spearman_corr2, spearman_p_value2 = stats.spearmanr(before2, after2)


    t_stat2, p_value2 = stats.ttest_rel(before3, after3)
    spearman_corr3, spearman_p_value3 = stats.spearmanr(before3, after3)


    t_stat3, p_value3 = stats.ttest_rel(before4, after4)
    spearman_corr4, spearman_p_value4 = stats.spearmanr(before4, after4)

    print(f"Llama3 & BLEU & Before & {round(t_stat,2)} ({'%10.2e' % (p_value)}) & {round(spearman_corr1,2)} ({'%10.2e' % (spearman_p_value1)}) \\\\ ")

    print(f"Llama3 & BLEU & After & {round(t_stat1,2)} ({'%10.2e' % (p_value1)}) & {round(spearman_corr2,2)} ({'%10.2e' % (spearman_p_value2)}) \\\\ ")

    print(f"Llama3 & BERT & Before & {round(t_stat2,2)} ({'%10.2e' % (p_value2)}) & {round(spearman_corr3,2)} ({'%10.2e' % (spearman_p_value3)}) \\\\ ")

    print(f"Llama3 & BERT & After & {round(t_stat3,2)} ({'%10.2e' % (p_value3)}) & {round(spearman_corr4,2)} ({'%10.2e' % (spearman_p_value4)}) \\\\ ")

# Uncomment and run the tests with your CSV files
# test('normal_eval.csv')
# test('normal_eval2.csv')

# test2('results/results.csv', 'normal_eval.csv')
# test2('results/results.csv', 'normal_eval2.csv')


test_threshold('results/results.csv', 'normal_eval.csv', 'passages_13_scores.csv', 70)
test_threshold('results/results.csv', 'normal_eval2.csv', 'passages_13_scores.csv', 70)








