import re
import os
from tika import parser
#from tools.JsonCRUD import save


patterns = {
    "Alice's Adventures in Wonderland": [],
    "Dune": [
        [
            r"\n\n\u2014[A-Z\s]+\n\n",
            "\n\n\u00a0\n\n\u2014THE STOLEN JOURNALS(\n\n\u00a0)+",
        ],
        [
            r"\n\n\u2014[A-Z\s]+\n\n",
            "\n\n\u00a0\n\n\u2014THE STOLEN JOURNALS(\n\n\u00a0)+",
        ],
        [
            r"\n\n\u2014[A-Z\s]+\n\n",
            "\n\n\u00a0\n\n\u2014THE STOLEN JOURNALS(\n\n\u00a0)+",
        ],
        [
            r"\n\n\u2014[A-Z\s]+\n\n",
            "\n\n\u00a0\n\n\u2014THE STOLEN JOURNALS(\n\n\u00a0)+",
        ],
        [
            r"\n\n\u2014[A-Z\s]+\n\n",
            "\n\n\u00a0\n\n\u2014THE STOLEN JOURNALS(\n\n\u00a0)+",
        ],
        [
            r"\n\n\u2014[A-Z\s]+\n\n",
            "\n\n\u00a0\n\n\u2014THE STOLEN JOURNALS(\n\n\u00a0)+",
        ],
    ],
    "Harry Potter": [
        [
            r"HP\t1\t-\tHarry\tPotter\tand\tthe\nSorcerer's\tStone",  # \n\n\n\nCHAPTER([A-Z',-]{2,50}\s)+
            r"\n\n\n\nd{1,6}\n\n" r"\n\nCHAPTER[\sA-Z]+\s+[A-Z'\s !,-]+\s+",
        ],
        [
            r"P a g e\s*\|\s*\d+\s*Harry Potter and the Chamber of Secrets .{5,30} Rowling",
            r"\n\n \n\n \n\n [A-Z' !,-]+",
        ],
        [r"\n\n\n\n\d+\n\n", r"\n\nCHAPTER [A-Z]+\n\n[A-Z' !,-]+"],
        [
            r"C H A P T E R [yA-Z' !,-]+ \s+\d+\s+[A-Z' !,-]+",
            r"\n\n\n\n[\sA-Z' !,-]+ \n\n \d+",
        ],
        [
            r"C H A P T E R [yA-Z' !,-]+ \s+\d+\s+[A-Z' !,-]+",
            r"\n\n\n\n[\sA-Z' !,-]+\n\n \d+",
        ],
        [
            r"\n\n \n\n \n\n[\sA-Z' !,-]+\n\n",
            r"P a g e\s*\|\s*\d+\s*Harry Potter and the Half Blood Prince .{5,30} Rowling",
        ],
        [
            r"\n\n\d+\n\n\n\nChapter \d+\n\n[a-zA-Z' !,-]{5,50}\n\n",
            r"\n\n\d+\n\n\n\nChapter \d+\n\n\w+\n\n",
            r"\n\n\d+\n\n\n\nChapter \d+\n\n",
            r"\n\n\d+\n\n\n\n[\u2019a-zA-Z' !,-]{5,50}\n\n",
            
        ],
    ],
    "The Hitchhiker's Guide to the Galaxy": [
        [
            r"\n\n\n\n(\d )+\s*\/[A-Z\s]+\s*\n\n",
            r"\n\n\n\nT H E  H I T C H H I K E R ' S  G U I D E  T O  T H E  G A L A X Y  /[\d\s]+\n\n",
        ]
    ],
    "The Hunger Games": [["\n\n\n\n \n\n\d+ \n \n\n"]],
    "The Lord of the Rings": [
        [
            r"\n\n\n\n[A-Z' !,-]+ \d+ \n\n",
            r"\n\n\n\n\d+ [A-Z' ,-]+\n\n",
            r"\n\n\n\nChapter \d+ \n\n[^a-z]+\n\n",
            r"\s\s\s\s.{20,200}\s\sThe Lord of the Rings \s\s\s\s\.\s\s\s\sBOOK [A-Z ]+\s\s\s\s\.",
        ]
    ],
    "Twilight": [
        [
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n.{5,15}\n\n",
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n",
        ],
        [
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n.{5,15}\n\n",
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n",
        ],
        [
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n.{5,15}\n\n",
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n",
        ],
        [
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n.{5,15}\n\n",
            r"\d+\s*\|\s*\sP a g e  .{5,30} Stephanie Meyer \n\n",
        ],
    ],  # re.compile(r"^[A-Z\s]+$", re.DOTALL)
}


boundaries = {
    "Alice's Adventures in Wonderland": [
        [
            "Alice was beginning to get very tired of sitting",
            "remembering her own child-life, and the happy summer days.",
        ]
    ],
    "Dune": [
        ["When the ghola", "fortune and most miraculous privilege."],
        [
            "A spot of light appeared on the deep red rug",
            "he said, “and he was always the stronger.”",
        ],
        [
            "IN THE week before their departure to Arrakis",
            "nothing can stand in their path.”",
        ],
        [
            "Muad’dib’s Imperial reign generated more historians",
            "base of the massif and its Place of Safety.",
        ],
        [
            "he three people running northward through moon shadows",
            "old story. You’ll find it all in my journals.”",
        ],
        [
            "“Taraza told you, did she not, that we have",
            "There was no answer but then she had not really expected an answer.",
        ],
    ],
    "Harry Potter": [
        ["Mr. and Mrs. Dursley", "of fun with Dudley this summer.…"],
        [
            "Not for the first time, an argument had broken out",
            "through the gateway to the Muggle world.",
        ],
        [
            "Harry Potter was a highly unusual boy in many ways. For one thing",
            "him, for what looked like a much better summer than the last.",
        ],
        ["he villagers of Little", "would have to meet it when it did."],
        [
            "he hottest day of the summer so far was drawing to a close and",
            "Petunia, and Dudley hurrying along in his wake.",
        ],
        [
            "It was nearing midnight and the Prime Minister was",
            "there was still one last golden day of peace left to enjoy with Ron and Hermione.",
        ],
        [
            "he two men appeared out of nowhere, a few yards apart",
            "The scar had not pained Harry for nineteen years. All was well.",
        ],
    ],
    "The Hitchhiker's Guide to the Galaxy": [
        [
            "Far out in the uncharted backwaters",
            'choose to collapse on Betelgeuse Seven".',
        ]
    ],
    "The Hunger Games": [
        [
            "When I wake up, the other side of the bed is cold.",
            "moment when I will finally have to let go.",
        ]
    ],
    "The Lord of the Rings": [
        [
            "This book is largely concerned with Hobbits",
            "He drew a deep breath. ‘Well, I’m back,’ he said.",
        ]
    ],
    "Twilight": [
        ["My mother drove me to the airport", "once more to my throat."],
        [
            "I was ninety-nine point nine percent sure I was",
            "my fate, with my destiny solidly at my side.",
        ],
        [
            "All our attempts at subterfage had been in vain",
            "letting Jacob Black disappear behind me.",
        ],
        ["No one is staring at you", "perfect piece of our forever."],
    ],
}


def make_pattern(start, end):
    # Convert start and end to regex patterns that ignore spaces and newlines
    start_pattern = r"\s*".join(re.escape(word) for word in start.split())
    end_pattern = r"\s*".join(re.escape(word) for word in end.split())
    # Combine the start and end patterns with the content in between
    return re.compile(start_pattern + r"(.*?)" + end_pattern, re.DOTALL)


def extract_book_content(book_name, index, text):
    # Check if the book name is in the dictionary
    if book_name not in boundaries:
        raise ValueError("Book name not found in the boundaries dictionary.")

    # Get the list of boundary pairs for the book
    start, end = boundaries[book_name][index]

    content_list = []
    pattern = make_pattern(start, end)
    match = pattern.search(text)
    if match:
        content_list.append(start + match.group(1) + end)

    # Join all the extracted content into a single string
    full_content = " ".join(content_list)

    return full_content


def filterRegex(book_name, index, text):
    removed_strings = []  # List to store removed strings

    for pattern in patterns[book_name][index]:
        reg = re.compile(pattern, re.DOTALL)
        found = reg.findall(text)
        removed_strings.extend(found)
        text = re.sub(reg, " ", text)
    # save(removed_strings, "removed.json")
    for f in found:
        if len(f) > 100:
            print("WARNING" + str(len(f)))
    return text.strip()


def normalize_spaces(text):
    # Replace all whitespace characters (\t, \n, \r, etc.) with a space
    text = re.sub(r"\s+", " ", text)
    # Remove leading and trailing spaces
    text = text.strip()
    return text


def loadBook(pdf_path):
    raw = parser.from_file(pdf_path)
    return raw["content"]

    # new_content = removePageNumbers(content)
    # new_content = removeParagraph(new_content)
    # new_content = separate_into_sentences(new_content)


def iterate_over_folders(root_folder):
    books = {}
    # Iterate over all files and subdirectories in the root folder
    for root, dirs, files in os.walk(root_folder):
        for folder in dirs:
            folder_path = os.path.join(root, folder)
            for filename in os.listdir(folder_path):
                filepath = os.path.join(folder_path, filename)
                if filepath.lower().endswith(".pdf"):
                    if folder in books:
                        books[folder].append(filepath)
                    else:
                        books[folder] = [filepath]

    return books
