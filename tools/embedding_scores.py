import json
import csv

def json_to_csv(json_file, csv_file):
    with open(json_file, 'r') as f:
        data = json.load(f)

    rows = []

    for book, characters in data.items():
        for character, passages in characters.items():
            for passage_info in passages:
                score = passage_info['score']
                rows.append([book, character, score])

    with open(csv_file, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(["Book Name", "Character Name", "Score"])
        writer.writerows(rows)

# Example usage
json_file = 'passages_13_scores.json'
csv_file = 'passages_13_scores.csv'
json_to_csv(json_file, csv_file)
