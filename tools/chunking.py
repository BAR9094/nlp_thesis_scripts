
import re
import os
from tika import parser 
from nltk.tokenize import sent_tokenize
import nltk

from langchain.text_splitter import NLTKTextSplitter
from langchain.text_splitter import SpacyTextSplitter
import spacy.cli
#from JsonCRUD import read, save

spacy.cli.download("en_core_web_sm")

#nltk.download('punkt')

# text_splitter = NLTKTextSplitter()
# docs1 = text_splitter.split_text(text)


def separate_into_sentences(text):
    sentences = sent_tokenize(text)
    return sentences

def spacy_chunking(text):
    text_splitter = SpacyTextSplitter(chunk_size=1000, max_length=3_000_000)
    return text_splitter.split_text(text)

    # new_content = #pdf_into_sentences(pdf_path)
    # total = []
    
    # ln = 0
    # block = ""
    # for sentence in new_content:
    #     if ln + len(sentence) < max_length:
    #         ln += len(sentence)
    #         block += sentence
    #     else:
    #         total.append(block)
    #         block = sentence  # Start new block with the current sentence
    #         ln = len(sentence) 
    # total.append(block)
    # res = []
    # for block in total:
    #     chunks = 
    #     res += chunks
    


def getFilteredText(list, substring):
    filtered_list = [item for item in list if substring in item]
    return filtered_list

def base_chunking(list, limit, alpha):
    size = len(list)
    intro_index = round(alpha * size)

    total = list[0:intro_index]
    if intro_index < limit:
        rest_length = size - intro_index
        rest_of_text = list[intro_index:]
        rest_needed_length = limit - intro_index
        n = rest_length//rest_needed_length
        if n < 1:
            n = 1
        total.extend(rest_of_text[::n])
    return total[:limit]

def base_chunk_multiple(string, substring):
    filtered_list = []
    list = separate_into_sentences(string)
    for item in list:
        passages = getFilteredText(item, substring)
        filtered_list.extend(passages)
    filtered_list = base_chunking(filtered_list, 1000, 0.1)
    return filtered_list




# def iterate_over_folders(root_folder):
#     books = {}
#     # Iterate over all files and subdirectories in the root folder
#     for root, dirs, files in os.walk(root_folder):
#         for folder in dirs:
#             folder_path = os.path.join(root, folder)
#             for filename in os.listdir(folder_path):
#                 filepath = os.path.join(folder_path, filename)
#                 if filepath.lower().endswith(".pdf"):
#                     if folder in books:
#                         books[folder].append(filepath)
#                     else:
#                         books[folder] = [filepath]

#     return books



#data = pdf_into_chunks("texts\Alice's Adventures in Wonderland\Alice.txt")
#save({"data": data}, "literatur_chunked.json")
