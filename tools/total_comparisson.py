import pandas as pd
import os

def process_and_sort(firstcsv, secondcsv):
    # Check if the CSV files exist
    if not os.path.exists(firstcsv) or not os.path.exists(secondcsv):
        print(f"Error: One or both of the files '{firstcsv}' or '{secondcsv}' do not exist.")
        return
    
    # Read the CSV files
    data1 = pd.read_csv(firstcsv)
    data2 = pd.read_csv(secondcsv)
    
    # Merge dataframes on the 'Character' column to find the intersection
    merged_data = pd.merge(data1, data2, on='Character', suffixes=('_1', '_2'))
    
    # Calculate improvement using the intersected data
    merged_data['F1c'] = merged_data['F1_1'] - merged_data['F1_2']
    merged_data['hc'] = merged_data['h_1'] - merged_data['h_2']
    merged_data['wF1c'] = merged_data['wF1_1'] - merged_data['wF1_2']
    merged_data['whc'] = merged_data['wh_1'] - merged_data['wh_2']

    # Sort based on improvement values
    sorted_by_F1c = merged_data.sort_values(by='F1c')
    sorted_by_hc = merged_data.sort_values(by='hc')
    sorted_by_wF1c = merged_data.sort_values(by='wF1c')
    sorted_by_whc = merged_data.sort_values(by='whc')

    # Create output filenames
    base_name = os.path.splitext(firstcsv)[0]
    output_F1_filename = f"{base_name}_comparisonF.csv"
    output_h_filename = f"{base_name}_comparisonh.csv"
    output_wF1_filename = f"{base_name}_comparisonwF.csv"
    output_wh_filename = f"{base_name}_comparisonwh.csv"

    # Save sorted data to new CSV files
    sorted_by_F1c.to_csv(output_F1_filename, index=False)
    sorted_by_hc.to_csv(output_h_filename, index=False)
    sorted_by_wF1c.to_csv(output_wF1_filename, index=False)
    sorted_by_whc.to_csv(output_wh_filename, index=False)

# Example usage:
process_and_sort("normal_eval.csv", "results/results.csv")
process_and_sort("normal_eval2.csv", "results/results.csv")
