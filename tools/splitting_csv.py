import pandas as pd
import numpy as np
import os

# Read the original CSV file
name = 'passages_13_scores'
s = "Score"
df = pd.read_csv(name + ".csv")

# Define the number of bins and range
num_bins = 10  # Example: number of bins
min_value = df[s].min()  # Or specify a fixed minimum value
max_value = df[s].max()  # Or specify a fixed maximum value

# Calculate bin edges
bin_edges = np.linspace(min_value, max_value, num_bins + 1)

# Calculate bin midpoints (means)
bin_midpoints = [(bin_edges[i] + bin_edges[i + 1]) / 2 for i in range(num_bins)]

# Create a directory to save the results if it doesn't exist
results_dir = 'results'
os.makedirs(results_dir, exist_ok=True)

# Initialize a dictionary to collect data
all_binned_percentages = {}

# Process each category
categories = df['Book'].unique()

# Create a DataFrame to hold all categories' data
combined_df = pd.DataFrame({'Score Interval': bin_midpoints})

for category in categories:
    # Filter data for the current category
    filtered_df = df[df['Book'] == category]
    
    # Bin the scores and count occurrences
    counts, _ = np.histogram(filtered_df[s], bins=bin_edges)
    
    # Calculate total number of scores for this category
    total_count = filtered_df[s].count()
    
    # Calculate percentage for each bin
    percentages = (counts / total_count) * 100
    
    # Add percentages to the combined DataFrame
    combined_df[category] = percentages

# Set the 'Score Interval' as the index
combined_df.set_index('Score Interval', inplace=True)

# Save to CSV with comma delimiter
filename = f'{results_dir}/{name}_binned_percentages_{s}.csv'
combined_df.to_csv(filename, sep=',', index=True)

print(f'Saved combined binned percentage data to "{filename}"')
