import json

def save(dict, name):

    try:
        with open(name, 'r') as json_file:
            existing_data = json.load(json_file)
    except FileNotFoundError:
        # If the file doesn't exist, create an empty dictionary
        existing_data = {}
        # Update the dictionary
    existing_data.update(dict)

    # Write the updated dictionary back to the JSON file
    with open(name, 'w') as json_file:
        json.dump(existing_data, json_file)

def read(dict_path):
    data = {}
    with open(dict_path, 'r', encoding='utf-8') as file:
        # Create a CSV reader object
        data = json.load(file)
            
    return data