import json
import csv

from JsonCRUD import read, save
from nltk.tokenize import sent_tokenize
#from preProcessing import filterAllTexts



def json_to_csv(json_file, csv_file):
    with open(json_file, 'r') as f:
        data = json.load(f)

    with open(csv_file, 'w', newline='') as f:
        writer = csv.writer(f)

        # Write header
        writer.writerow(['Book', 'Character', 'F1', 'h', 'wF1', 'wh'])

        # Write rows
        for book in data.keys():
            for character, metrics in data[book].items():
                print(metrics)
                row = [book, character]
                for key in metrics:
                    row.append(metrics.get(key, 'N/A'))
                writer.writerow(row)

def json_to_csv2(json_file, csv_file):
    with open(json_file, 'r') as f:
        data = json.load(f)

    with open(csv_file, 'w', newline='') as f:
        writer = csv.writer(f)

        # Write header
        writer.writerow(['F1', 'h', 'wF1', 'wh'])

        # Write rows
        for book in data.keys():
            for character, metrics in data[book].items():
                print(metrics)
                row = [book, character, metrics]

                writer.writerow(row)

def count_data(json):
    json = read(json)
    for book in json:
        print(book, len(json[book].keys()))


def count_missings(json, generated_json, missing):
    j = read(json)
    gj = read(generated_json)
    #bks = read("kolo.json")
    data = {}
    for bi, book in enumerate(j):
        if not book in gj:
            continue
        else:
            data[book] = {}
        for ci, char in enumerate(j[book]):
            if not char in gj[book]:
                data[book][char] = None#filterAllTexts(bks[book], char)
                print(book, char)
    save(data, missing)


def count_chunk_sentences():
    data = read("literatur_chunked_fine.json")
    for book in data:
        for subbook in data[book]:
            for chunk in subbook:
                print(len(sent_tokenize(chunk)))

count_data("other/fandom.json")
count_data("fandoms.json")
# count_chunk_sentences()
#json_to_csv2('bin_counts.json', 'bin_counts.csv')
#json_to_csv('normal_eval2.json', 'normal_eval2.csv')
count_missings("other/gen_fand4.json", "normal_eval.json", "missing2.json")
count_missings( "normal_eval.json", "other/gen_fand4.json", "missing.json")