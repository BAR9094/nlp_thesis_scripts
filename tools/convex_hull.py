import pandas as pd
import numpy as np
from scipy.spatial import ConvexHull
import csv
import matplotlib.pyplot as plt

# Function to compute convex hulls for each score category, save vertices to separate CSV files, and plot results
def compute_save_and_plot_convex_hulls(input_csv, output_csv_base):
    # Load data from CSV
    df = pd.read_csv(input_csv)
    
    # Plot setup
    plt.figure(figsize=(12, 10))
    
    # Iterate over each unique category in the 'Book' column
    for book_id in df['Book'].unique():
        # Extract points for the current book
        df_book = df[df['Book'] == book_id]
        points_f1_wf1 = df_book[['F1', 'wF1']].values
        points_h_wh = df_book[['h', 'wh']].values
        
        # Function to compute convex hull for a set of points and save to a CSV file
        def compute_hull_and_save(points, label):
            hull = ConvexHull(points)
            hull_vertices = points[hull.vertices]
            
            # Close the hull by adding the first vertex at the end
            hull_vertices_closed = np.vstack((hull_vertices, hull_vertices[0]))
            
            # Save vertices with book_id to CSV
            output_csv = f"convex/{book_id}_{label}_{output_csv_base}"
            with open(output_csv, 'w', newline='') as csvfile:
                csvwriter = csv.writer(csvfile)
                csvwriter.writerow(['Book', 'X', 'Y'])
                for vertex in hull_vertices_closed:
                    csvwriter.writerow([book_id, vertex[0], vertex[1]])
            print(f"Convex hull vertices for Book {book_id} ({label}) saved to {output_csv}")

            # Plot original points and convex hull
            plt.scatter(points[:, 0], points[:, 1], label=f'Book {book_id} {label} Points')
            plt.plot(hull_vertices_closed[:, 0], hull_vertices_closed[:, 1], 'r-', lw=2, label=f'Book {book_id} {label} Convex Hull')
            plt.fill(hull_vertices_closed[:, 0], hull_vertices_closed[:, 1], 'r', alpha=0.2)
        
        # Compute and save hulls for both sets of points separately
        compute_hull_and_save(points_f1_wf1, 'F1_wF1')
        compute_hull_and_save(points_h_wh, 'h_wh')
    
    # Add labels, legend, and show plot
    plt.title('Convex Hulls for Different Books')
    plt.xlabel('X-axis')
    plt.ylabel('Y-axis')
    plt.legend()
    plt.grid(True)
    plt.show()

# Example usage
input_csv = 'normal_eval.csv'  # Replace with your CSV file path
output_csv_base = 'convex_hull.csv'  # Base name for output files
compute_save_and_plot_convex_hulls(input_csv, output_csv_base)
