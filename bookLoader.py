
from tools.JsonCRUD import save, read
# import numpy as np
# from sacrebleu.metrics import BLEU
# from bert_score import score
from tools.preProcessing import filterRegex, loadBook, iterate_over_folders, extract_book_content, normalize_spaces
from tools.chunking import spacy_chunking
def loadBooksRaw():
    data = {}
    books = iterate_over_folders("texts")
    for name in books.keys():
        _list = books[name]
        data = []
        for index, sub_book in enumerate(_list):
            #.............
            book = loadBook(sub_book)
            processed = extract_book_content(name, index, book)
            # processed = filterRegex(name, index, processed)
            # processed = normalize_spaces(processed)
            # print(len(processed)/len(book))

            data.append(processed)
        save({"data" : data}, f"{name}_raw.json")

def loadBooks():
    data = {}
    books = iterate_over_folders("texts")
    for name in books.keys():
        
        _list = books[name]
        for index, sub_book in enumerate(_list):
            #.............
            print(f"processing {name} {index}")
            book = loadBook(sub_book)
            processed = extract_book_content(name, index, book)
            processed = filterRegex(name, index, processed)
            processed = normalize_spaces(processed)
            print(len(processed)/len(book))
            if name in data:
                data[name].append(processed)
            else:
                data[name] = [processed]

    save(data, "training_data/books.json")

def chunkBooks():
    books = read("training_data/books.json")
    books = books
    
    for b in books:
        res = []
        for i, sub in enumerate(books[b]):
            print(b, i)
            res.extend(spacy_chunking(sub))
        save({"data" : res}, f"training_data/{b}_spacy_fine.json")

    
#loadBooksRaw()
#loadBooks()
chunkBooks()