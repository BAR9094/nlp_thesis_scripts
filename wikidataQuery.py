import requests
from tools.fandomArticleScrape import scrapeFandom, checkUrl
from tools.JsonCRUD import read, save
import os

sparql_query = """
SELECT ?typeLabel ?item ?itemLabel ?fandom ?bookLabel ?url ?fandomStatement
WHERE 
{
  ?item wdt:P31 wd:Q3658341 .
  ?item wdt:P1441 ?book .
  VALUES ?type {wd:Q8261 wd:Q7725634 wd:Q47461344 wd:Q1667921 wd:Q13593966 wd:Q108465955 wd:Q108317211 wd:Q116476516}.
  ?book wdt:P31 ?type.
  ?item p:P6262 ?fandomStatement.
  ?fandomStatement ps:P6262 ?fandom.
  ?fandomStatement pq:P407 wd:Q1860 .
  
  

  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
}


"""
sqparql_query2 = """
SELECT
  ?item
  ?fandom
  ?fandomStatement
  ?characterName

WHERE {{
  ?item wdt:P31 wd:Q3658341.
  ?item p:P6262 ?fandomStatement.
  ?fandomStatement ps:P6262 ?fandom.
   ?fandomStatement pq:P1810 ?characterName.

  BIND(STRBEFORE(?fandom, ":") AS ?firstHalf).
  FILTER (?firstHalf = "{0}").

  #SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
}}
"""
used_books = ["Alice's Adventures in Wonderland", "The Lord of the Rings", "Harry Potter", "The Hunger Games", "Dune", "Twilight", "The Hitchhiker's Guide to the Galaxy"]
fandoms =  ["aliceinwonderland", "lotr", "harrypotter", "thehungergames", "dune", "twilightsaga", "hitchhikers"]


def query_wikidata(query):
    endpoint_url = "https://query.wikidata.org/sparql"
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36',
        'Accept': 'application/json'
    }
    params = {
        'query': query,
        'format': 'json'
    }

    response = requests.get(endpoint_url, headers=headers, params=params)
    data = response.json()
    return data

def get_fandom_urls():
# Process the result
    result = query_wikidata(sparql_query)
    k = {}
    for item in result['results']['bindings']:
        lable = item['bookLabel']['value']
        d = item['fandom']['value'].split(":")
        builded_url = {"name": item['itemLabel']['value'] ,"url" : f"https://{d[0]}.fandom.com/wiki/{d[1]}"}
        if lable in k:
            # If yes, append the value to the existing list
            k[lable].append(builded_url)
        else:
            # If no, create a new list with the value
            k[lable] = [builded_url]
    return k
    # new_dict = {key: len(value) for key, value in k.items()}

    # sorted_items = sorted(new_dict.items(), key=lambda x: x[1])

    # # Create a new ordered dictionary from the sorted items
    # sorted_dict = dict(sorted_items)

    # # star_trek_names = []

    # # # Print the sorted dictionary
    # print(sorted_dict) #k["The Star Trek Book"]
    #save(k, "nanir.json")

def get_fandom_urls_for_one_page(url):
    formatted_query = sqparql_query2.format(url)
    result = query_wikidata(formatted_query)
    k = {}
    for item in result['results']['bindings']:

        d = item['fandom']['value'].split(":")
        name = item['characterName']['value']
        k[d[1]] = {"name" : name , "url" : f"https://{d[0]}.fandom.com/wiki/{d[1]}"}

    return k

    # new_dict = {key: len(value) for key, value in k.items()}

    # sorted_items = sorted(new_dict.items(), key=lambda x: x[1])

    # # Create a new ordered dictionary from the sorted items
    # sorted_dict = dict(sorted_items)

    # # star_trek_names = []

    # # # Print the sorted dictionary
    # print(sorted_dict) #k["The Star Trek Book"]
    #save(k, "nanir.json")

def filterForUsedBooks(used, dict):
    new_dict = {}
    for k in dict.keys():
        if k in used:
            new_dict[k] = dict[k]
    return new_dict

def checkArticleLength(article):
    length = 0
    if len(article) > 0:
        for p in article:
            length += len(p)

    return length


def printFandomsAmount(z):
    total = 0
    for k in z.keys():
        total += len(z[k].keys())
        print(k, len(z[k].keys()))
    print("total", total)

def compareMissingFandoms(incomplete, base):
    total = 0
    for book in base.keys():
        for character in base[book].keys():
            if not character in incomplete[book]:
                total += 1
                print(f"{book}{character} is missing!")
    print("total missing", total)

def printFandomsLength(z):
    total = 0
    for k in z.keys():
        total += len(z[k])
        print(k, len(z[k]))
    print("total", total)

def loadTxt(book):
    dune_characters = {}
    _path = f'addInfo/{book}.txt'
    if os.path.exists(_path):
       #print("yes")
        with open(_path) as file:
            for line in file:
                name, key, url = line.strip().split(' - ')
                dune_characters[key] = {"name" : name , "url" : url}
    else:

        print("no additional Characters added")
    return dune_characters

def removeBadFandoms(fandoms):
    # Create a new dictionary with only the keys that do not contain "/XD" or "/DE"
    filtered_fandoms = {key: value for key, value in fandoms.items() if "/XD" not in key and "/DE" not in key}
    return filtered_fandoms

#fetch all fandoms urls
def getFandomUrls():
    all_fandoms = {}
    for index, book in enumerate(used_books):
        
        fandom = get_fandom_urls_for_one_page(fandoms[index])
        own_fandom = loadTxt(book)
        fandom.update(own_fandom)
        fandom = removeBadFandoms(fandom)
        print(book, len(fandom.keys()))
        all_fandoms[book]= fandom
    return all_fandoms
# fetch actual fandom
def getFandoms(urls):
    storage = {}
    for index, name in enumerate(urls.keys()):
        print(index, len(urls.keys()))
        book = urls[name]
        storage[name] = {}
        for char_key in book.keys():
            _url = book[char_key]["url"]
            _name = book[char_key]["name"]
            _check = checkUrl(_url)
            if _check != None:
                res = scrapeFandom(_check)
                _length = checkArticleLength(res)
                _data = {"data" : res, "length" : _length}


                if _name in storage[name]:
                    if _length > storage[name][_name]["length"]:
                        storage[name][_name] = _data
                else:
                    storage[name][_name]  = _data
            else:
                print(f"couldn't fetch url: {_url}")
    return storage
#average fadnom length
def getAverageFandomLength(dic):
    av = 0
    for key, value in dic.items():
        avg = 0
        for k, v in value.items():
            avg += v["length"]
        print(avg/len(value.items()))
        av += avg/len(value.items())
    print(av/len(dic.items()))

def limitfandomSize(dat, threshold):
    dict = {}
    for key, value in dat.items():
        dict[key] = {}
        for k, v in value.items():
            first = len(v["data"][0])
            total = v["length"] 
            print(first, total)
            if abs(first - threshold) < abs(total - threshold):
                dict[key][k] = [v["data"][0]]
            else:
                dict[key][k] = v["data"]
    return dict






#FetchFandomUrls
# urls = getFandomUrls()
# fandoms = getFandoms(urls)
# fandoms = limitfandomSize(fandoms, 10000)
# save(fandoms, "fandoms.json")


fandoms = read("fandoms.json")
printFandomsAmount(fandoms)









#compareMissingFandoms(ratta, fan)
#print(get_fandom_urls_for_one_page("lotr"))






#(.+) - (.+/)(.+)
#$3 - $2$3
