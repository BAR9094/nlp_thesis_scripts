
from tools.JsonCRUD import save, read
import numpy as np
from sacrebleu.metrics import BLEU
from bert_score import score
#from tools.preProcessing import pdf_into_chunks, iterate_over_folders, pdf_into_sentences
import pandas as pd
from io import StringIO
bleu = BLEU(lowercase=True, effective_order=True) 



def normal():
    originals = read('fandoms.json')
    generated = read('gen_with_encoding.json')
    dc = {}

    for book in generated:
        dc[book] = {}
        for character in generated[book]:
            gen_with = generated[book][character]["with"]
            gen_without = generated[book][character]["without"]
            #for index, name in enumerate(generated[book]):
            original_fandom = ''.join(originals[book][character])

            #print(index, len(generated[book]))
            # print(len(original_fandom))
            #print(len(generated_fandom))

            wh = bleu.sentence_score(gen_with, [original_fandom])
            wP, wR, wF1 = score([gen_with], [original_fandom], lang='de', model_type="microsoft/deberta-xlarge-mnli", verbose=True)
                
            h = bleu.sentence_score(gen_without, [original_fandom])
            P, R, F1 = score([gen_without], [original_fandom], lang='de', model_type="microsoft/deberta-xlarge-mnli", verbose=True)
            
            dc[book][character] = {"wF1":wF1.item(), "wh":wh.score, "F1":F1.item(), "h":h.score}
    save(dc, "normal_eval.json")


def bins():
    dc = read('results.json')
    #bin_edges = np.linspace(0.3, 0.7, num=10)  # Define your bins here
    bin_edges = np.linspace(0.0, 13.0, num=10)
    # Initialize a defaultdict to store bin counts for each character
    character_bin_counts = {"wF1": {}, "wh": {}, "F1": {}, "h": {}}

    for book, characters in dc.items():
        for character, attributes in characters.items():
            for key, value in attributes.items():
                for bin_edge in bin_edges:
                    # Check if the value falls within the bin
                    if value <= bin_edge:
                        # Increment the count for this bin for this character
                        if bin_edge in character_bin_counts[key]:
                        
                            character_bin_counts[key][bin_edge] += 1
                        else:
                            character_bin_counts[key][bin_edge]  = 1
                        break

    save(character_bin_counts, "bin_counts.json")


def computeCharacteristics():
    total = pd.DataFrame()
    dfs = [pd.read_csv('results.csv'), pd.read_csv('results2.csv'), pd.read_csv('results3.csv'), pd.read_csv('results4.csv')]


    for df in dfs:
        # Specify the columns you want to analyze
        columns_to_analyze = ['F1', 'wF1']

        # Select only the specified columns
        selected_columns = df[columns_to_analyze]

        # Calculate the mean, median, and other statistics for the selected columns
        mean_values = selected_columns.mean()
        median_values = selected_columns.median()
        std_dev_values = selected_columns.std()
        min_values = selected_columns.min()
        max_values = selected_columns.max()
        q1_values = selected_columns.quantile(0.25)
        q3_values = selected_columns.quantile(0.75)
        iqr_values = q3_values - q1_values

        # Create a DataFrame to hold all statistics
        stats_df = pd.DataFrame({
            #'Mean': mean_values,
            'min': min_values,
            'median': median_values,
            'max': max_values,
            'q1': q1_values,
            'q3': q3_values,
            #'Standard Deviation': std_dev_values,
            #'Interquartile Range (IQR)': iqr_values
        })
        total = pd.concat([total, stats_df], ignore_index=True, sort=False)
    total.to_csv('statistics_results.csv', index=False, header=True)
    total = pd.DataFrame()
    for df in dfs:
        # Specify the columns you want to analyze
        columns_to_analyze = ['h', 'wh']

        # Select only the specified columns
        selected_columns = df[columns_to_analyze]

        # Calculate the mean, median, and other statistics for the selected columns
        mean_values = selected_columns.mean()
        median_values = selected_columns.median()
        std_dev_values = selected_columns.std()
        min_values = selected_columns.min()
        max_values = selected_columns.max()
        q1_values = selected_columns.quantile(0.25)
        q3_values = selected_columns.quantile(0.75)
        iqr_values = q3_values - q1_values

        # Create a DataFrame to hold all statistics
        stats_df = pd.DataFrame({
            #'Mean': mean_values,
            'min': min_values,
            'median': median_values,
            'max': max_values,
            'q1': q1_values,
            'q3': q3_values,
            #'Standard Deviation': std_dev_values,
            #'Interquartile Range (IQR)': iqr_values
        })
        total = pd.concat([total, stats_df], ignore_index=True, sort=False)
    total.to_csv('statistics_results2.csv', index=False, header=True)
    
# Call the function
#computeCharacteristics()
normal()