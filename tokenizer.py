import json
import requests
import sys
import numpy as np
# import os
# # Get the current directory of script_a.py
# current_dir = os.path.dirname(os.path.abspath(__file__))

# # Append the parent directory to the system path
# parent_dir = os.path.dirname(current_dir)
# sys.path.append(parent_dir)

from tools.preProcessing import filterAllTexts
from tools.JsonCRUD import read, save

def tokenize(url, headers, help, character):
    query2_wh = "[INST]Write a summary in the style of a fandom article about the character {character} in the given text passages: \n {help}[/INST]"
    content = query2_wh.format(character=character, help=help)
    data = {"content": content}
    response = requests.post(url, headers=headers, data=json.dumps(data))
    return response.json(), content

def tokenizeAll():
    fandom = read('new_fandom.json')
    bks = read("kolo.json")
    url = "http://localhost:18881/tokenize"
    headers = {"Content-Type": "application/json"}
    data = []
    for book in fandom:
        if not book in bks:
            continue
        print(book)
        for character in fandom[book]:
            passages = filterAllTexts(bks[book], character)
            res, content = tokenize(url, headers, passages, character)
            print(len(res['tokens']), len(content), len(res['tokens']) / len(content))
            data.append(len(res['tokens']) / len(content))
    print(np.average(data))
    print(np.mean(data))
    # 0.27966780117435963
    # 0.27966780117435963
    
tokenizeAll()


