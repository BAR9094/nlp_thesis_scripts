import requests
import json
from tools.JsonCRUD import read, save

url = "http://127.0.0.1:11434/api/generate"

query_woh = "[INST]Write a summary about the character {character} in the book {book}.[/INST]"
query_wh = "[INST]Write a summary about the character {character} in the given text passages: \n {help}[/INST]"

headers = {"Content-Type": "application/json"}

def query_without_help(book, character):
    #gemma2:9b-instruct-fp16
    #llama3:8b-instruct-fp16
    payload = {
        "model": "gemma2:9b-instruct-fp16",
        "prompt": query_woh.format(character=character, book=book),
        "options": {"temperature": 0.0},
        "stream": False
    }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    return response.json()

def query_with_help(help, character):
    payload = {
        "model": "gemma2:9b-instruct-fp16",
        "prompt": query_wh.format(character=character, help=help),
        "options": {"temperature": 0.0},
        "stream": False
    }
    response = requests.post(url, data=json.dumps(payload), headers=headers)
    return response.json()





# ####################################################
# # passages = filterAllTexts(bks["Harry Potter"], "Seamus Finnigan")
# # print(passages)
# ####################################################
# # LOTR Boromir
# # Harry Potter 2 Remus Lupin 0
# # Harry Potter 2 Minerva McGonagall 1
# # Harry Potter 2 Katie Bell 27


def generateFandomsWithEncoding(fandoms, file_name, from_book=1, from_character=0):
    da = read("passages_13.json")
    generated_fandoms = read(file_name)
    for index, book in enumerate(fandoms):
        if index >= from_book:
            if not book in generated_fandoms:
                generated_fandoms[book] = {}
            for ind, character in enumerate(fandoms[book]):
                if ind >= from_character and not character in generated_fandoms[book]:
                    print("trying", book, index , character, ind)
                    # passages = filterAllTexts(books[book], character)
                    passages = da[book][character]
                    res1 = query_without_help(book, character)
                    res2 = query_with_help(passages, character)
                    generated_fandoms[book][character] = {}
                    generated_fandoms[book][character]["without"] = res1['response']
                    generated_fandoms[book][character]["with"] = res2['response']
                    generated_fandoms[book][character]["passages"] =  passages
                    save(generated_fandoms, file_name)
                    print(index, ind)
                    print(book, character)

# fandoms = read('new_fandom.json')
# books= read("kolo.json")
# generateFandoms(fandoms, books, "gen_fand4.json")


fandoms = read('fandoms.json')
generateFandomsWithEncoding(fandoms, "gen_with_encoding2.json")